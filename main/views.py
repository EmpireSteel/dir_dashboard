from django.shortcuts import render, redirect
from . import store_class
from .models import Message, Incident
from django.contrib.auth.decorators import login_required
from .ad import checkUserInAD
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from .forms import LoginForm
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from datetime import datetime
import os
from django.http import HttpResponse



# Create your views here.


@csrf_exempt
def download_stoped_products_report(request, sap):
    with open('reports/{0}/stoped_products_report.csv'.format(sap), 'rb') as fp:
        data = fp.read()
    filename = '{}_stoped_products_report_{}.csv'.format(sap, str(datetime.now().date()))
    response = HttpResponse(content_type="application/")
    response['Content-Disposition'] = 'attachment; filename=%s' % filename # force browser to download file
    response.write(data)
    return response


@csrf_exempt
def get_stoped_products(request, sap):
    stoped_products = store_class.get_stoped_products(sap)
    data = {'header': stoped_products['header'],
            'body': stoped_products['body'],
            'thead': stoped_products['thead'],
            'tbody': stoped_products['tbody']}
    return JsonResponse(data)


@csrf_exempt
def download_minus_products_report(request, sap):
    with open('reports/{0}/minus_products_report.csv'.format(sap), 'rb') as fp:
        data = fp.read()
    filename = '{}_minus_products_report_{}.csv'.format(sap, str(datetime.now().date()))
    response = HttpResponse(content_type="application/")
    response['Content-Disposition'] = 'attachment; filename=%s' % filename # force browser to download file
    response.write(data)
    return response


@csrf_exempt
def get_minus_products(request, sap):
    minus_products = store_class.get_minus_products(sap)
    data = {'header': minus_products['header'],
            'body': minus_products['body'],
            'thead': minus_products['thead'],
            'tbody': minus_products['tbody']}
    return JsonResponse(data)

@csrf_exempt
def download_alcohol_errors_report(request, sap):
    with open('reports/{0}/alcohol_errors_report.csv'.format(sap), 'rb') as fp:
        data = fp.read()
    filename = '{}_alcohol_errors_report_{}.csv'.format(sap, str(datetime.now().date()))
    response = HttpResponse(content_type="application/")
    response['Content-Disposition'] = 'attachment; filename=%s' % filename # force browser to download file
    response.write(data)
    return response

@csrf_exempt
def download_overdue_products_report(request, sap):
    with open('reports/{0}/overdue_products_report.csv'.format(sap), 'rb') as fp:
        data = fp.read()
    filename = '{}_overdue_products_report_{}.csv'.format(sap, str(datetime.now().date()))
    response = HttpResponse(content_type="application/")
    response['Content-Disposition'] = 'attachment; filename=%s' % filename # force browser to download file
    response.write(data)
    return response


@csrf_exempt
def download_low_saled_products_report(request, sap):
    with open('reports/{0}/low_saled_products_report.csv'.format(sap), 'rb') as fp:
        data = fp.read()
    filename = '{}_low_saled_products_report_{}.csv'.format(sap, str(datetime.now().date()))
    response = HttpResponse(content_type="application/")
    response['Content-Disposition'] = 'attachment; filename=%s' % filename # force browser to download file
    response.write(data)
    return response


@csrf_exempt
def download_sellers_perfom_report(request, sap):
    with open('reports/{0}/sellers_perfom_report.csv'.format(sap), 'rb') as fp:
        data = fp.read()
    filename = '{}_sellers_perfom_report_{}.csv'.format(sap, str(datetime.now().date()))
    response = HttpResponse(content_type="application/")
    response['Content-Disposition'] = 'attachment; filename=%s' % filename # force browser to download file
    response.write(data)
    return response


@csrf_exempt
def get_sellers_perfom(request, sap):
    sellers_perfom = store_class.get_sellers_perfom(sap)
    data = {'header': sellers_perfom['header'],
            'body': sellers_perfom['body'],
            'thead': sellers_perfom['thead'],
            'tbody': sellers_perfom['tbody']}
    return JsonResponse(data)


@csrf_exempt
def get_services_net(request, sap):
    services_net = store_class.get_services_net(sap)
    data = {'header': services_net['header'],
            'tooltip': services_net['tooltip']}
    return JsonResponse(data)


@csrf_exempt
def get_low_saled_products(request, sap):
    low_saled_products = store_class.get_low_saled_products(sap)
    data = {'header': low_saled_products['header'],
            'body': low_saled_products['body'],
            'thead': low_saled_products['thead'],
            'tbody': low_saled_products['tbody']}
    return JsonResponse(data)


@csrf_exempt
def get_alcohol_errors(request, sap):
    alcohol_errors = store_class.get_alcohol_errors(sap)
    data = {'header': alcohol_errors['header'],
            'body': alcohol_errors['body'],
            'modal': alcohol_errors['modal']}
    return JsonResponse(data)

@csrf_exempt
def get_rx(request, sap):
    rx = store_class.get_rx(sap)
    data = {'header': rx['header'],
            'services_tooltip': rx['services']['tooltip'],
            'business_tooltip': rx['business']['tooltip'],
            'body': rx['body']}
    return JsonResponse(data)


@csrf_exempt
def get_alcohol_documents(request, sap):
    get_alcohol_documents = store_class.get_alcohol_documents(sap)
    data = {'header': get_alcohol_documents['header'],
            'body': get_alcohol_documents['body']}
    return JsonResponse(data)


@csrf_exempt
def get_services_alcohol(request, sap):
    services_alcohol = store_class.get_services_alcohol(sap)
    data = {'header': services_alcohol['header'],
            'tooltip': services_alcohol['tooltip']}
    return JsonResponse(data)


@csrf_exempt
def get_cashless(request, sap):
    cashless = store_class.get_cashless(sap)
    data = {
            'services_header': cashless['services']['header'],
            'services_tooltip': cashless['services']['tooltip'],}
    return JsonResponse(data)


@csrf_exempt
def get_overdue_products(request, sap):
    products = store_class.get_overdue_products(sap)
    data = {'header': products['header'],
            'body': products['body'],
            'thead': products['thead'],
            'tbody': products['tbody']}
    return JsonResponse(data)
 

@csrf_exempt
def get_business_revenue(request, sap):
    business_revenue = store_class.get_business_revenue(sap)
    data = {'header': business_revenue['header'],
            'body': business_revenue['body'],
            'tooltip': business_revenue['tooltip']}
    return JsonResponse(data)
 

@csrf_exempt
def get_loyalty(request, sap):
    loyalty = store_class.get_loyalty(sap)
    data = {
            'services_header': loyalty['services']['header'],
            'services_tooltip': loyalty['services']['tooltip'],}
    return JsonResponse(data)


@login_required
def index(request):
    if request.method == 'POST':
        search = request.POST.get('store').upper()
        store = store_class.get_full_sap(search)
        return redirect('dashboard', store)
    else:
        return render(request, 'main/index.html')
    
def get_poses(request, sap):
    poses = store_class.get_poses(sap)
    return JsonResponse(poses)


def get_kso(request, sap):
    kso = store_class.get_kso(sap)
    return JsonResponse(kso)


def get_scales(request, sap):
    scales = store_class.get_scales(sap)
    return JsonResponse(scales)
    


@login_required
def dashboard(request, host):
    if request.method == 'POST':
        search = request.POST.get('store').upper()
        store = store_class.get_full_sap(search)
        return redirect('dashboard', store)
    else:
        store = store_class.get_full_name(host)
        message = Message.objects.filter(store=host).first()
        sap = host.split('-')[-1]
        incidents = Incident.objects.filter(store=host)
        return render(request, 'main/dashboard.html', {
                                                    'full_sap': host,
                                                    'sap': sap,
                                                    'full_name': store,
                                                    'message': message, 
                                                    'incidents': incidents, 
                                                    })


def sign_in(request):
    if request.user.is_authenticated:
        return redirect(index)
    else:
        if request.method == 'POST':
            form = LoginForm(request.POST)
            if form.is_valid():
                username = form.cleaned_data['username'].lower()
                password = form.cleaned_data['password']
                if username == 'admin':
                    user = authenticate(request, username=username, password=password)
                    if user is not None:
                        login(request, user)
                        return redirect(index)
                else:
                    if '@' in username:
                        username = username.split('@')[0]
                    if checkUserInAD(username+'@x5.ru', password):
                        user = authenticate(request, username=username, password=password)
                        if user is not None:
                            login(request, user)
                            return redirect(index)
                        else:
                            if User.objects.filter(username=username).exists():
                                user = User.objects.get(username__exact=username)
                                user.set_password(password)
                                user.save()
                                login(request, user)
                                return redirect(index)
                            else:
                                user = User.objects.create_user(username, username+'@x5.ru', password)
                                user.save()
                                login(request, user)
                                return redirect(index)
                    else:
                        return redirect(sign_in)
            else:
                return redirect(sign_in)
        else:
            form =LoginForm()
    return render(request, 'main/sign_in.html', {'form':form})


def do_logout(request):
    logout(request)
    return redirect(sign_in)