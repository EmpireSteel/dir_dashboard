# Generated by Django 2.1.7 on 2019-04-02 12:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_incident_current_status'),
    ]

    operations = [
        migrations.RenameField(
            model_name='incident',
            old_name='status',
            new_name='criticality',
        ),
        migrations.AlterField(
            model_name='incident',
            name='current_status',
            field=models.CharField(default='', max_length=255),
        ),
    ]
