from django.urls import path
from . import views


urlpatterns = [
    path('sign_in/', views.sign_in, name='sign_in'),
    path('store/<str:sap>/download_low_saled_products_report/', views.download_low_saled_products_report, name='download_low_saled_products_report'),
    path('store/<str:sap>/get_low_saled_products/', views.get_low_saled_products, name='get_low_saled_products'),

    path('store/<str:sap>/download_stoped_products_report/', views.download_stoped_products_report, name='download_stoped_products_report'),
    path('store/<str:sap>/get_stoped_products/', views.get_stoped_products, name='get_stoped_products'),
    path('store/<str:sap>/download_minus_products_report/', views.download_minus_products_report, name='download_minus_products_report'),
    path('store/<str:sap>/get_minus_products/', views.get_minus_products, name='get_minus_products'),

    path('store/<str:sap>/download_overdue_products_report/', views.download_overdue_products_report, name='download_overdue_products_report'),
    path('store/<str:sap>/get_overdue_products/', views.get_overdue_products, name='get_overdue_products'),
    path('store/<str:sap>/download_alcohol_errors_report/', views.download_alcohol_errors_report, name='download_alcohol_errors_report'),
    path('store/<str:sap>/get_alcohol_errors/', views.get_alcohol_errors, name='get_alcohol_errors'),
    path('store/<str:sap>/download_sellers_perfom_report/', views.download_sellers_perfom_report, name='download_sellers_perfom_report'),
    path('store/<str:sap>/get_sellers_perfom/', views.get_sellers_perfom, name='get_sellers_perfom'),
    path('store/<str:sap>/get_scales/', views.get_scales, name='get_scales'),
    path('store/<str:sap>/get_poses/', views.get_poses, name='get_poses'),
    path('store/<str:sap>/get_kso/', views.get_kso, name='get_kso'),
    path('store/<str:sap>/get_services_net/', views.get_services_net, name='get_services_net'),
    path('store/<str:sap>/get_business_revenue/', views.get_business_revenue, name='get_business_revenue'),
    path('store/<str:sap>/get_rx/', views.get_rx, name='get_rx'),
    path('store/<str:sap>/get_services_alcohol/', views.get_services_alcohol, name='get_services_alcohol'),
    path('store/<str:sap>/get_loyalty/', views.get_loyalty, name='get_loyalty'),
    path('store/<str:sap>/get_cashless/', views.get_cashless, name='get_cashless'),
    path('store/<str:sap>/get_alcohol_documents/', views.get_alcohol_documents, name='get_alcohol_documents'),
    path('logout/', views.do_logout, name='logout'),
    path('store/<str:host>/', views.dashboard, name='dashboard'),
    path('', views.index, name='index'),
]

