from pyzabbix import ZabbixAPI
import os
import cx_Oracle
import time
from datetime import datetime
import requests


bf_rule1 = ('1', '2', 'B')
bf_rule2 = ('4000', '4001', '4002', '4003', '4004', '4005', '4006')



def get_full_name(store):
    db = get_interfaces(store)
    conn = cx_Oracle.connect("MONITORING/1AAA2BB3D@//{}:1521/ORCL".format(db), encoding="UTF-8", nencoding="UTF-8")
    cur = conn.cursor()
    cur.execute("""select distinct(cli.name) as NAME, NVL( substr(clie.ext_string, 7,4), clie.ext_string)as SAP
        from sdd.client cli
        left join sdd.department dep on dep.is_host = 1
        left join sdd.client_ext clie on clie.id_client = dep.id_client and clie.id_company = dep.id_company and clie.ext_name = 'SAPCODE'
        where cli.id_client = dep.id_client and cli.name is not null
        """
    )
    response = cur.fetchall()
    conn.close()
    return response[0][0]
            
def get_kso(store):
    db = get_bd_ip(store)
    ksos = {'header': 'dark', 'kso':[]}
    offline = 0
    conn = cx_Oracle.connect("MONITORING/1AAA2BB3D@//{}:1521/ORCL".format(db), encoding="UTF-8", nencoding="UTF-8")
    cur = conn.cursor()
    cur.execute('''select 
                        NVL(sum(case when ch.pay_type = 1 and ch.operation <> 2 then ch.total end), 0) as "Нал",
                        NVL(sum(case when ch.pay_type = 2 and ch.operation <> 2 then ch.total end), 0) as "Безнал",
                        NVL(sum(ch.total), 0) as "Сумма" , 
                        ch.id_paydesk,
                        cp.localname
                        from cash.checkheader ch
                        join cash.checkpaydesk cp on ch.id_paydesk = cp.id_paydesk
                        where trunc(ch.date_time) = trunc(sysdate) and ch.operation not in (3, 4, 13, 14, 15, 16, 1000, 1001, 1002, 6000, 6001) and ch.pay_type <> 0 and ch.id_paydesk > 900
                        group by ch.id_paydesk, cp.localname
                        order by ch.id_paydesk''')
    response = cur.fetchall()
    conn.close()
    for i in response:
            kso = {'name': str(i[3] - 900), 'typ': 'success', 'cash': 0, 'cashless': 0, 'summa': 0}
            kso['cash'] = i[0]
            kso['cashless'] = i[1]
            kso['summa'] = i[2]
            if ping(i[4]):
                kso['typ'] = 'card bg-success text-white my-1'
            else:
                offline += 1
                kso['typ'] = 'card bg-dark text-white my-1'
            ksos['kso'].append(kso)
    if ksos['kso']:
        if offline / len(ksos['kso']) <= 0.2:
            ksos['header'] = 'card-header bg-success text-white'
        elif 0.2 < offline / len(ksos['kso']) < 0.6:
            ksos['header'] = 'card-header bg-warning text-white'
        elif offline / len(ksos['kso']) >= 0.6:
            ksos['header'] = 'card-header bg-danger text-white'
    else:
        ksos['header'] = 'card-header bg-danger text-white'
    return ksos


def get_posses_zabbix(store):
    zabbix = ZabbixAPI('http://zabbix-head.x5.ru/')
    zabbix.login('store_card_api', '12345')
    find = zabbix.do_request('host.get', {
            'search': {
                'name': store
            },
            'selectInterfaces': ['ip', 'dns'],
            'output': 'extend'
        })['result']
    zabbix.session.close()
    poses = [{'ip': i['ip'], 'name': i['dns']} for i in find[0]['interfaces'] if i['dns'].startswith('POS')]
    return poses
    


def get_poses(store):
    db = get_bd_ip(store)
    zabbix_poses = get_posses_zabbix(store)
    poses = {'header': 'dark', 'poses':[]}
    offline = 0
    conn = cx_Oracle.connect("MONITORING/1AAA2BB3D@//{}:1521/ORCL".format(db), encoding="UTF-8", nencoding="UTF-8")
    cur = conn.cursor()
    cur.execute('''select 
                        NVL(sum(case when ch.pay_type = 1 and ch.operation <> 2 then ch.total end), 0) as "Нал",
                        NVL(sum(case when ch.pay_type = 2 and ch.operation <> 2 then ch.total end), 0) as "Безнал",
                        NVL(sum(case when ch.pay_type = 9 and ch.operation <> 2 then ch.total end), 0) as "Смешаные",
                        NVL(sum(case ch.operation when 2 then ch.total end), 0) as "Возвраты",
                        NVL(sum(ch.total), 0) as "Сумма" , 
                        id_paydesk
                        from cash.checkheader ch
                        where trunc(ch.date_time) = trunc(sysdate) and ch.operation not in (3, 4, 13, 14, 15, 16, 1000, 1001, 1002, 6000, 6001) and ch.pay_type <> 0
                        group by id_paydesk''')
    response = cur.fetchall()
    conn.close()
    for i in zabbix_poses:
        pos = {'name': int(i['name'].split('-')[0][3:]), 'typ': 'success', 'cash': 0, 'cashless': 0, 'mixed': 0, 'returns': 0, 'summa': 0, 'ip': i['ip']}
        for j in response:
            if get_full_sap(store).startswith('Giper'):
                if str(j[5])[0] != '9':
                    if j[5] > 100:
                        if pos['name'] == j[5] - int(str(j[5])[0] + '00'):
                            pos['cash'] = j[0]
                            pos['cashless'] = j[1]
                            pos['mixed'] = j[2]
                            pos['returns'] = j[3]
                            pos['summa'] = j[4]
                    else:
                        if pos['name'] == j[5]:
                            pos['cash'] = j[0]
                            pos['cashless'] = j[1]
                            pos['mixed'] = j[2]
                            pos['returns'] = j[3]
                            pos['summa'] = j[4]
            elif get_full_sap(store).startswith('Super'):
                if pos['name'] == j[5]:
                    pos['cash'] = j[0]
                    pos['cashless'] = j[1]
                    pos['mixed'] = j[2]
                    pos['returns'] = j[3]
                    pos['summa'] = j[4]
        if ping(i['ip']):
            pos['typ'] = 'card bg-success text-white my-1'
        else:
            offline += 1
            pos['typ'] = 'card bg-dark text-white my-1'
        poses['poses'].append(pos)
    if poses['poses']:
        if offline / len(poses['poses']) <= 0.2:
            poses['header'] = 'card-header bg-success text-white'
        elif 0.2 < offline / len(poses['poses']) < 0.6:
            poses['header'] = 'card-header bg-warning text-white'
        elif offline / len(poses['poses']) >= 0.6:
            poses['header'] = 'card-header bg-danger text-white'
    else:
        poses['header'] = 'card-header bg-danger text-white'
    return poses


def get_scales(store):
    db = get_bd_ip(store)
    scales = {'header': 'dark', 'scales':[]}
    offline = 0
    typ = []
    conn = cx_Oracle.connect("MONITORING/1AAA2BB3D@//{}:1521/ORCL".format(db), encoding="UTF-8", nencoding="UTF-8")
    cur = conn.cursor()
    cur.execute('''select st.scale_type_name, s.port, s.dt_scale_load, sysdate from sdd.scale_type st join sdd.scales s on s.id_scale_type = st.id_scale_type 
                        where to_char(s.dt_scale_load, 'DD.MM.YYYY')<>'01.01.0001' ''')
    response = cur.fetchall()
    conn.close()
    for i in response:
        scale = {'name': i[0].split('(')[0].capitalize(), 'delta': i[3].timestamp() - i[2].timestamp(), 'typ':'dark'}
        ip = i[1].split('.')
        for j in range(len(ip)):
            while ip[j].startswith('0'):
                ip[j] = ip[j][1:]
        scale['ip'] = '.'.join(ip)
        if not ping(scale['ip']):
            scale['typ'] = 'card bg-dark text-white my-1'
            offline += 1
        elif scale['delta'] >= 43200:
                scale['typ'] = 'card bg-danger text-white my-1'
        elif scale['delta'] >= 21600:
                scale['typ'] = 'card bg-warning text-white my-1'
        else:
            scale['typ'] = 'card bg-success text-white my-1'
        typ.append(scale['typ'])
        scales['scales'].append(scale)
    if 'danger' in typ or offline/len(scales['scales']) >= 0.5 :
        scales['header'] = 'card-header bg-danger text-white'
    elif 'warning' in typ or 'dark' in typ or offline/len(scales['scales']) >= 0.25:
        scales['header'] = 'card-header bg-warning text-white'
    else:
        scales['header'] = 'card-header bg-success text-white'
    return scales
            

def get_business_revenue(store):
    db = get_bd_ip(store)
    business_revenue = {'cash': {'today': 1, 'last_week': 1, 'arrow': '', 'color': ''}, 'cashless': {'today': 1, 'last_week': 1, 'arrow': '', 'color': ''},
                    'mixed': {'today': 1, 'last_week': 1, 'arrow': '', 'color': ''}, 'returns': {'today': 1, 'last_week': 1, 'arrow': '', 'color': ''},
                    'revenue': {'today': 1, 'last_week': 1, 'arrow': '', 'color': ''}}
    conn = cx_Oracle.connect("MONITORING/1AAA2BB3D@//{}:1521/ORCL".format(db), encoding="UTF-8", nencoding="UTF-8")
    cur = conn.cursor()
    cur.execute('''select 
                    NVL(sum(case when ch.pay_type = 1 and ch.operation <> 2 then ch.total end), 0) as "Нал",
                    NVL(sum(case when ch.pay_type = 2 and ch.operation <> 2 then ch.total end), 0) as "Безнал",
                    NVL(sum(case when ch.pay_type = 9 and ch.operation <> 2 then ch.total end), 0) as "Смешаные",
                    NVL(sum(case ch.operation when 2 then ch.total end), 0) as "Возвраты",
                    NVL(sum(ch.total), 0) as "Сумма" 
                    from cash.checkheader ch where trunc(ch.date_time) = trunc(sysdate) 
                                                    and ch.operation not in (3, 4, 13, 14, 15, 16, 1000, 1001, 1002, 6000, 6001) 
                                                    and ch.pay_type <> 0''')
    response = cur.fetchall()
    business_revenue['cash']['today'] = int(response[0][0])
    business_revenue['cashless']['today'] = int(response[0][1])
    business_revenue['mixed']['today'] = int(response[0][2])
    business_revenue['returns']['today'] = int(response[0][3])
    business_revenue['revenue']['today'] = int(response[0][4])
    cur.execute('''select 
            NVL(sum(case when ch.pay_type = 1 and ch.operation <> 2 then ch.total end), 0) as "Нал",
            NVL(sum(case when ch.pay_type = 2 and ch.operation <> 2 then ch.total end), 0) as "Безнал",
            NVL(sum(case when ch.pay_type = 9 and ch.operation <> 2 then ch.total end), 0) as "Смешаные",
            NVL(sum(case ch.operation when 2 then ch.total end), 0) as "Возвраты",
            NVL(sum(ch.total), 0) as "Сумма" 
            from cash.checkheader ch where ch.date_time < sysdate-7 and trunc(ch.date_time) = trunc(sysdate-7) 
                                            and ch.operation not in (3, 4, 13, 14, 15, 16, 1000, 1001, 1002, 6000, 6001) 
                                            and ch.pay_type <> 0''')
    response = cur.fetchall()
    conn.close()
    business_revenue['cash']['last_week'] = int(response[0][0])
    business_revenue['cashless']['last_week'] = int(response[0][1])
    business_revenue['mixed']['last_week'] = int(response[0][2])
    business_revenue['returns']['last_week'] = int(response[0][3])
    business_revenue['revenue']['last_week'] = int(response[0][4])
    if business_revenue['revenue']['last_week'] != 0:
        if business_revenue['revenue']['today'] / business_revenue['revenue']['last_week'] <= 0.5:
            business_revenue['revenue']['arrow'] = 'fas fa-exclamation-triangle'
            business_revenue['revenue']['color'] = 'card bg-danger text-white my-1'
        elif business_revenue['revenue']['today'] / business_revenue['revenue']['last_week'] <= 0.75:
            business_revenue['revenue']['arrow'] = 'fas fa-long-arrow-alt-down'
            business_revenue['revenue']['color'] = 'card bg-danger text-white my-1'
        elif business_revenue['revenue']['today'] / business_revenue['revenue']['last_week'] < 0.9:
            business_revenue['revenue']['arrow'] = 'fas fa-long-arrow-alt-down'
            business_revenue['revenue']['color'] = 'card bg-warning text-white my-1'
        elif 0.9 <= business_revenue['revenue']['today'] / business_revenue['revenue']['last_week'] <= 1.1:
            business_revenue['revenue']['arrow'] = 'far fa-check-circle'
            business_revenue['revenue']['color'] = 'card bg-success text-white my-1'
        else:
            business_revenue['revenue']['arrow'] = 'fas fa-long-arrow-alt-up'
            business_revenue['revenue']['color'] = 'card bg-success text-white my-1'
    else:
        business_revenue['revenue']['arrow'] = 'fas fa-long-arrow-alt-up'
        business_revenue['revenue']['color'] = 'success'
    business_revenue['header'] = business_revenue['revenue']['color']
    business_revenue['body'] = '₽ {} | {} <i class="{} float-right"></i>'.format(business_revenue['revenue']['today'], business_revenue['revenue']['last_week'], business_revenue['revenue']['arrow'])
    business_revenue['tooltip'] = 'Наличные: {} | {}<br>Безналичные: {} | {}<br>Смешанные: {} | {}<br>Возвраты: {} | {}'.format(business_revenue['cash']['today'], business_revenue['cash']['last_week'], business_revenue['cashless']['today'], business_revenue['cashless']['last_week'], business_revenue['mixed']['today'], business_revenue['mixed']['last_week'], business_revenue['returns']['today'], business_revenue['returns']['last_week'])
    return business_revenue


def get_loyalty(store):
    hostid = get_hostid(store)
    loyalty = {'business': {}, 'services': {}}
    comarch = 0
    zabbix = ZabbixAPI('http://zabbix-head.x5.ru/')
    zabbix.login('store_card_api', '12345')
    find = zabbix.do_request('item.get', {
                'hostids': hostid,
                'webitems': 'true',
                'search': {'key_': 'web.test.rspcode[Comarch_check,Comarch]'},
                'output': ['lastvalue']
            })['result']
    if find:
        comarch = find[-1]['lastvalue']
    if comarch == '200':
        loyalty['services']['header'] = 'card bg-success text-white my-1'
        loyalty['services']['tooltip'] = 'Сервис "Лояльность" доступен'
    elif comarch == '0':
        loyalty['services']['header'] = 'card bg-dark text-white my-1'
        loyalty['services']['tooltip'] = 'Статус службы неизвестен'
    else:
        loyalty['services']['header'] = 'card bg-danger text-white my-1' 
        loyalty['services']['tooltip'] = 'Сервис "Лояльность" недоступен'
    return loyalty



def get_cashless(store):
    hostid = get_hostid(store)
    cashless = {'services':{}, 'business':{}}
    # self.cashless['business'] = {'good_k': 0, 'bad_k': 0, 'body': '', 'header': ''}
    zabbix = ZabbixAPI('http://zabbix-head.x5.ru/')
    zabbix.login('store_card_api', '12345')
    find = zabbix.do_request('item.get', {
                'hostids': hostid,
                'webitems': 'true',
                'search': {'key_': 'net.tcp.port[192.168.17.162,666]'},
                'output': ['lastvalue']
            })['result']
    zabbix.session.close()
    if find:
        sberbank = find[-1]['lastvalue']
    else: 
        sberbank = '-1'
    if sberbank == '1':
        cashless['services']['header'] = 'card bg-success text-white my-1'
        cashless['services']['tooltip'] = 'Сбербанк доступен'
    elif sberbank == '-1':
        cashless['services']['header'] = 'card bg-dark text-white my-1' 
        cashless['services']['tooltip'] = 'Сбербанк статус неизвестен'
    else:
        cashless['services']['header'] = 'card bg-danger text-white my-1' 
        cashless['services']['tooltip'] = 'Сбербанк недоступен'
    return cashless
    

def get_alcohol_errors(store):
    db = get_bd_ip(store)
    conn = cx_Oracle.connect("MONITORING/1AAA2BB3D@//{}:1521/ORCL".format(db), encoding="UTF-8", nencoding="UTF-8")
    cur = conn.cursor()
    alcohol_errors = {'count': 0, 'header': get_services_alcohol(store)['header']}
    cur.execute('''select 
                    t.date_time,
                    d.ext_number,
                    d.ext_string,
                    e.ext_string,
                    l.id_plu,
                    ee.ext_string
                from cash.checkheader_ext d
                join cash.checkheader t on t.ID_department = d.id_department and t.id_paydesk = d.id_paydesk and t.id_header = d.id_header and t.date_time > trunc(sysdate)
                left join cash.checkdetail l on l.ID_department = t.id_department and l.id_paydesk = t.id_paydesk and l.id_header = t.id_header
                join cash.checkdetail_ext e on l.ID_department = e.id_department and l.id_paydesk = e.id_paydesk and l.id_header = e.id_header and l.id_detail = e.id_detail and e.ext_name like 'ALC_RAR'
                join cash.checkdetail_ext ee on l.ID_department = ee.id_department and l.id_paydesk = ee.id_paydesk and l.id_header = ee.id_header and l.id_detail = ee.id_detail and ee.ext_name like 'EAN'
                join sdd.client_ext cli on cli.ext_name = 'SAPCODE' and cli.id_company = (select dep.id_company from sdd.department dep where dep.is_host = 1)
                and cli.id_client = (select dep.id_client from sdd.department dep where dep.is_host = 1)
                where d.ext_name in  ('EGAIS_ERROR_0001', 'AGENT_MARKCHECK_ERROR_0001') 
                and l.id_detail = e.id_detail
                and l.qty = 0
                order by t.date_time desc''')
    response = cur.fetchall()
    conn.close()
    alcohol_errors['count'] = len(response)
    if alcohol_errors['header'] == 'card bg-danger text-white my-1':
        status = 'fas fa-exclamation-triangle float-right pt-1'
    else:
        if alcohol_errors['count'] > 0:
            alcohol_errors['header'] = 'card bg-warning text-white my-1'
            status = 'fas fa-exclamation-triangle float-right pt-1'
        else:
            alcohol_errors['header'] = 'card bg-success text-white my-1'
            status = 'far fa-check-circle float-right pt-1'
    alcohol_errors['body'] = "{} <i class='{}'></i>".format(alcohol_errors['count'], status)
    alcohol_errors['errors'] = {}
    write_report('alcohol_errors_report', ['Дата', 'Код ошибки', 'Расшифровка ошибки', 'Акцизная марка', 'PLU', 'Штрих-код'], response, store)
    for i in response:
        if i[2] not in alcohol_errors['errors']:
            alcohol_errors['errors'][i[2]] = {'name': i[2].split(': ')[-1].replace('.', '').replace('"', '').capitalize(), 'count': 0} 
        alcohol_errors['errors'][i[2]]['count'] += 1
    if alcohol_errors['errors']:
        alcohol_errors['modal'] = ['{}:  {}'.format(alcohol_errors['errors'][i]['name'], alcohol_errors['errors'][i]['count']) for i in alcohol_errors['errors']]
        alcohol_errors['modal'] = '<br>'.join(alcohol_errors['modal'])
    else:
        alcohol_errors['modal'] = 'Ошибок нет'
    return alcohol_errors
      
def get_services_alcohol(store):
    db = get_bd_ip(store)
    services_alcohol = {'header': ''}
    conn = cx_Oracle.connect("MONITORING/1AAA2BB3D@//{}:1521/ORCL".format(db), encoding="UTF-8", nencoding="UTF-8")
    cur = conn.cursor()
    cur.execute("select p_value from cash.paramspaydesk where PARAM like 'EGAISMAINMODULEPATH'")
    response = cur.fetchall()
    conn.close()
    utm_url = response[0][0]
    status = requests.get(utm_url)
    if status.status_code == 200:
        services_alcohol['header'] = 'card bg-success text-white my-1'
        services_alcohol['tooltip'] = 'Статус УТМ: УТМ доступен'
    else:
        services_alcohol['header'] = 'card bg-danger text-white my-1'
        services_alcohol['tooltip'] = 'Статус УТМ: УТМ недоступен'
    return services_alcohol

def get_rx(store):
    db = get_bd_ip(store)
    full_sap = get_full_sap(store)
    rx = {'services':{}, 'business': {}}
    conn = cx_Oracle.connect("MONITORING/1AAA2BB3D@//{}:1521/ORCL".format(db), encoding="UTF-8", nencoding="UTF-8")
    cur = conn.cursor()
    cur.execute("Select sysdate from dual")
    response = cur.fetchall()
    rx['time'] = response[0][0]
    conn.close()
    if full_sap.startswith('Giper'):
        conn = cx_Oracle.connect("HELPDESK/Zz123456@//msk-dpro-ora090.x5.ru:1521/PX", encoding="UTF-8", nencoding="UTF-8")
    elif full_sap.startswith('Super'):        
        conn = cx_Oracle.connect("HELPDESK/Zz123456@//msk-dpro-ora144.x5.ru:1521/prd1", encoding="UTF-8", nencoding="UTF-8")
    cur = conn.cursor()
    cur.execute("SELECT nq_code FROM pe.properties WHERE id_department = '{}'".format(store))
    response = cur.fetchall()
    rx['nq'] = response[0][0]
    cur.execute("SELECT ip FROM px.department WHERE id_department = {}".format(rx['nq']))
    response = cur.fetchall()
    rx['ip'] = response[0][0]
    cur.execute("SELECT status FROM px.sheet WHERE id_department = {} and dt_begin = trunc(sysdate-1)".format(rx['nq']))
    response = cur.fetchall()
    rx['business']['list_status'] = response[0][0]
    cur.execute("SELECT dt_close FROM ps.task WHERE id_department = {} and status in ('C', 'E') and type = 'activate' and sheet_date = to_char(sysdate - 1, 'yyyy-mm-dd') || ' 00:00:00'".format(rx['nq']))
    response = cur.fetchall()
    rx['business']['active_time'] = response[0][0]
    cur.execute("SELECT tm FROM ps.task WHERE id_department = {} and type = 'close' and sheet_date = to_char(sysdate - 1, 'yyyy-mm-dd') || ' 00:00:00'".format(rx['nq']))
    response = cur.fetchall()
    try:
        rx['business']['plan_close'] = response[0][0]
    except:
        rx['business']['plan_close'] = '0'

    cur.execute("SELECT dt_closed FROM px.sheet WHERE id_department = {} and dt_begin = trunc(sysdate-1)".format(rx['nq']))
    response = cur.fetchall()
    rx['business']['fact_close'] = response[0][0]

    cur.execute("SELECT count(*) FROM px.act WHERE nick = 'lost' AND id_sheet = (SELECT id_sheet FROM px.sheet WHERE id_department = {} AND dt_begin = trunc(sysdate-1))".format(rx['nq']))
    response = cur.fetchall()
    rx['business']['sp_created'] = response[0][0]

    cur.execute("SELECT count(*) FROM px.act WHERE nick = 'lost' AND is_approved = 1 AND id_sheet = (SELECT id_sheet FROM px.sheet WHERE id_department = {} AND dt_begin = trunc(sysdate-1))".format(rx['nq']))
    response = cur.fetchall()
    rx['business']['sp_signed'] = response[0][0]

    cur.execute("SELECT sum(dd.summ) FROM px.act a JOIN px.doc d ON d.id_act = a.id_act JOIN px.doc_detail dd ON dd.id_doc = d.id_doc WHERE a.nick = 'lost' AND a.is_approved = 1 AND a.id_sheet = (SELECT id_sheet FROM px.sheet WHERE id_department = {} AND dt_begin = trunc(sysdate-1))".format(rx['nq']))
    response = cur.fetchall()
    try:
        rx['business']['sp_summ'] = int(response[0][0])
    except:
        rx['business']['sp_summ'] = response[0][0]
    conn.close()
    try:
        if ping(self.rx['ip']):
            rx['services']['ping_status'] = 'Доступен'
            rx['services']['ping'] = True
        else:
            rx['services']['ping_status'] = 'Недоступен'
            rx['services']['ping'] = False
    except:
        rx['services']['ping_status'] = 'В базе нет данных по доступности сервера'
        rx['services']['ping'] = True
    if rx['business']['list_status'] == 'O':
        rx['business']['day_status'] = 'Открыт, магазин может выполнять только операции Приемка и Списание'
    elif rx['business']['list_status'] == 'A':
        rx['business']['day_status'] = 'Активен, магазин может выполнять любые операции'
    elif rx['business']['list_status'] == 'C':
        rx['business']['day_status'] = 'Закрыт, магазин может выполнять только операции Приемка и Списание'
    if not rx['services']['ping'] or (rx['business']['list_status'] == 'O' and datetime.now().replace(hour=10, minute=30, second=0, microsecond=0) > rx['time']):
        rx['header'] = 'card bg-danger text-white my-1'
    elif rx['services']['ping'] and ((rx['business']['list_status'] == 'O' and datetime.now().replace(hour=10, minute=30, second=0, microsecond=0) < rx['time']) or rx['business']['list_status'] == 'C'):
        rx['header'] = 'card bg-warning text-white my-1'
    elif rx['services']['ping'] and rx['business']['list_status'] == 'A':
        rx['header'] = 'card bg-success text-white my-1'
    else:
        rx['header'] = 'card bg-dark text-white my-1'
    rx['services']['tooltip'] = 'Статус сервера PX: {}'.format(rx['services']['ping_status']) 
    rx['business']['tooltip'] = 'Календарный день: {}<br>Время активации дня: {}<br>Время планового закрытия дня: {}<br>Время фактического закрытия дня: {}'.format(rx['business']['day_status'], rx['business']['active_time'], rx['business']['plan_close'], rx['business']['fact_close'])
    if rx['business']['sp_summ'] == None or rx['business']['sp_summ'] == 0:    
        rx['business']['sp_summ'] = 0 
        rx['body'] = '₽ ' + str(rx['business']['sp_summ']) + " <i class='far fa-check-circle float-right pt-1'></i>"
    else:
        rx['body'] = '₽ ' + str(rx['business']['sp_summ']) + " <i class='fas fa-exclamation-triangle float-right pt-1'></i>"
    return rx


def ping(ip):
    if os.system("fping " + ip) == 0:
        return True
    else:
        return False


def get_services_net(store):
    db = get_bd_ip(store)      
    services_net = {}
    ip = db.split('.')[:-1]
    ip.append('1')
    ip = '.'.join(ip)
    if ping(ip):
        services_net['header'] = 'card bg-success text-white my-1'
        services_net['tooltip'] = 'Маршрутизатор доступен'
    else:
        services_net['header'] = 'card bg-danger text-white my-1'
        services_net['tooltip'] = 'Маршрутизатор недоступен'
    return services_net


def write_report(name, header, data, store):
    if not os.path.exists('reports/'):
        os.mkdir('reports/')
    if not os.path.exists('reports/{}'.format(str(store))):
        os.mkdir('reports/{}'.format(str(store)))
    with open('reports/{0}/{1}.csv'.format(str(store), name), 'w', encoding='cp1251') as f:
        f.write(';'.join(header) + '\n')
        for i in data:
            f.write(';'.join([str(x).capitalize() for x in i]) + '\n')


def get_overdue_products(store):
    db = get_bd_ip(store)       
    products = {}
    conn = cx_Oracle.connect("MONITORING/1AAA2BB3D@//{}:1521/ORCL".format(db), encoding="UTF-8", nencoding="UTF-8")
    cur = conn.cursor()
    cur.execute("""
                SELECT Dd.Id_Item,
            i.shortname,
            Dd.Id_Header,
            Dd.Qty - sum(nvl(f.Qty, 0)) Free_Qty,
            min(dmr.ext_date) DT_MAX_REALIZATION
        FROM Sdd.Docdetail     Dd,
            Sdd.Docheader     Dh,
            Sdd.Fifolist2     f,
            sdd.docdetail_ext dmr,
            sdd.item          i
        WHERE Dh.Id_Department = sdd.dcutil.id_department
        AND Dd.Id_Item = i.id_item
        and i.id_item in (select id_item from sdd.itemlist where qty > 0)
        and i.Id_Sap_Group IN
            (SELECT Id_Itemgroup
                FROM (SELECT LEVEL AS Lvl, RPAD('_', LEVEL, '_') || g.Name, g.*
                        FROM Sdd.Sap_Itemgroup g
                        CONNECT BY PRIOR g.Id_Itemgroup = g.Id_Parent
                        START WITH g.Id_Itemgroup in
                                    ('FR0800000', 'FR0600000'))
                WHERE Lvl = 3)
        and i.Id_Sap_Group not IN ('FR0906009', 'FR0906008')
        AND Dd.Id_Department = Dh.Id_Department
        AND Dd.Id_Header = Dh.Id_Header
        and f.Id_Department(+) = Dd.Id_Department
        AND f.Id_Item(+) = Dd.Id_Item
        AND f.Src_Id_Header(+) = Dd.Id_Header
        and Dh.Qin + Dh.Qik - Dh.Qot - Dh.Qtt - Dh.Qsp - Dh.Qos > 0
        and dmr.id_department(+) = sdd.dcutil.id_department
        and dmr.ext_name(+) = 'DT_MAX_REALIZATION'
        and dmr.ext_date < sysdate
        and DMR.ID_HEADER(+) = dd.id_header
        and dmr.id_item(+) = dd.id_item
        and Dh.Status <> 'S'
        group by Dd.Id_Header, i.shortname, Dd.Id_Item, Dd.Qty
        having Dd.Qty - sum(nvl(f.Qty, 0)) > 0
        order by id_item
    """)
    response = cur.fetchall()
    conn.close()
    if response:
        products['header'] = 'card bg-danger text-white my-1'
        products['body'] = str(len(response)) + " <i class='fas fa-exclamation-triangle float-right pt-1'></i>"
    else:
        products['header'] = 'card bg-success text-white my-1'
        products['body'] = str(len(response)) + " <i class='far fa-check-circle float-right pt-1'></i>"
    products['thead'] = ['PLU', 'Найменование товара', 'Номер поставки', 'Кол-во просроченных продуктов', 'Годен до']
    products['tbody'] = response
    write_report('overdue_products_report', products['thead'], products['tbody'], store)
    return products

        

def get_alcohol_documents(store):
    db = get_bd_ip(store)
    alcohol_documents = {}
    conn = cx_Oracle.connect("MONITORING/1AAA2BB3D@//{}:1521/ORCL".format(db), encoding="UTF-8", nencoding="UTF-8")
    cur = conn.cursor()
    cur.execute("""
    select dep.nickname,substr(cli.ext_string, 7, 4) as SAP, depe.ext_string, ext.id_header, ext.ext_string as bufer, ext1.ext_string as TTN, 
                                buf.dt_created, buf.status, buf.note, cl.nickname as POSTAVSHIK
                                from sdd.bufheader_ext ext
                                join sdd.bufheader buf on buf.status in ('A', 'X', 'O') and buf.id_doctype = 8 and buf.id_header = ext.id_header
                                join sdd.bufheader_ext ext1 on ext1.id_header = ext.id_header and ext1.ext_name = 'TTN'
                                join sdd.department dep on dep.is_host = 1
                                join sdd.department_ext depe on depe.id_department = dep.id_department and depe.ext_name = 'FORMAT'
                                join sdd.client_ext cli on cli.id_company = dep.id_company and cli.id_client = dep.id_client and cli.ext_name = 'SAPCODE'
                                join sdd.client cl on cl.id_client = buf.id_client and cl.nickname is not null
                                where ext.ext_name = 'BUF_IDENT_ALCO' and ext.id_header in (select lk.id_header from sdd.bufdetail lk where lk.id_header = buf.id_header and lk.id_header = ext.id_header and lk.id_item in 
                                (select rr.id_item from sdd.itemlist_ext rr where rr.ext_name = 'ALC_IND' and rr.ext_number = 1))
                                group by dep.nickname,substr(cli.ext_string, 7, 4),depe.ext_string, ext.id_header, ext.ext_string, ext1.ext_string, buf.dt_created, buf.status, buf.note, cl.nickname
    """)
    response = cur.fetchall()
    conn.close()
    if response:
        alcohol_documents['header'] = 'card bg-danger text-white my-1'
        alcohol_documents['body'] = str(len(response)) + " <i class='fas fa-exclamation-triangle float-right pt-1'></i>"
    else:
        alcohol_documents['header'] = 'card bg-success text-white my-1'
        alcohol_documents['body'] = str(len(response)) + " <i class='far fa-check-circle float-right pt-1'></i>"
    return alcohol_documents
        
        

def get_bd_ip(store):
    store = get_full_sap(store)
    db = get_interfaces(store)
    return db


def get_full_sap(store):
    if store.upper().startswith("SUPER") or store.upper().startswith(bf_rule1) or store.upper() in bf_rule2:
        zabbix = ZabbixAPI('http://zabbix-head.x5.ru/')
        zabbix.login('store_card_api', '12345')
        find = zabbix.do_request('host.get', {
            'search': {
                'name': store
            },
            'monitored_hosts': 'true',
            'output': ['name']
        })['result']
        zabbix.session.close()
        if find:
            return find[0]['name']

def get_hostid(store):
    if store.upper().startswith("SUPER") or store.upper().startswith(bf_rule1) or store.upper() in bf_rule2:
        zabbix = ZabbixAPI('http://zabbix-head.x5.ru/')
        zabbix.login('store_card_api', '12345')
        find = zabbix.do_request('host.get', {
            'search': {
                'name': store
            },
            'monitored_hosts': 'true',
            'output': ['name']
        })['result']
        zabbix.session.close()
        if find:
            return find[0]['hostid']



def get_interfaces(store):
    interfaces = []
    zabbix = ZabbixAPI('http://zabbix-head.x5.ru/')
    zabbix.login('store_card_api', '12345')
    find = zabbix.do_request('host.get', {
                'search': {
                    'name': store
                },
                'selectInterfaces': ['ip', 'dns'],
                'output': 'extend'
            })['result']
    zabbix.session.close()
    interfaces = [{'ip': i['ip'], 'name': i['dns']} for i in find[0]['interfaces']]
    for i in interfaces:
        if i['name'].startswith('ORA'):
            return i['ip']
            # ip = '.'.join(i['ip'].split('.')[:-1]) 

def get_low_saled_products(store):
    db = get_bd_ip(store)
    products = {}
    conn = cx_Oracle.connect("MONITORING/1AAA2BB3D@//{}:1521/ORCL".format(db), encoding="UTF-8", nencoding="UTF-8")
    cur = conn.cursor()
    cur.execute("""
                select id_item, --plu
                shortname, --name
                summa, --сумма за 21 день
                average, --среднее кол. за сутки
                dc.qty, --за последний день
                weight --шт. или вес.
                from (select id_item,
                i.shortname,
                sum(qty) as summa,
                case i.weight
                    when 'W' then
                    round(sum(qty) / 21, 2)
                    else
                    round(sum(qty) / 21)
                end as average,
                case i.weight
                    when 'W' then
                    'КГ'
                    else
                    'ШТ'
                end as weight
                from sdd.docdetail dd
                inner join sdd.item i
                using (id_item)
                where dd.id_header in (select id_header
                                    from sdd.docheader
                                    where id_doctype = 21
                                    and dt_created > trunc(sysdate - 22)
                                    and dt_created < trunc(sysdate - 1)
                                    and status = '_')
                /*PLU которые участвовали в промо*/
                and id_item not in (select cd.plu
                                    from cash.canister c
                                    join cash.can_detail cd
                                using (v_plu, id_department)
                                where c.date_start > trunc(sysdate - 22)
                                    and c.date_end < trunc(sysdate - 1)
                                    and c.is_activity = 1
                                    and c.priz <> 99
                                    and cd.plu > 0
                                group by cd.plu)
                group by id_item, i.shortname, i.weight
                order by summa desc) ntl
                inner join sdd.docdetail dc
                using (id_item)
                where id_header = (select id_header
                        from sdd.docheader
                        where id_doctype = 21
                        and dt_created = trunc(sysdate - 1)
                        and status = '_')
                and dc.qty * 2 < average

    """)
    response = cur.fetchall()
    conn.close()
    if response:
        products['header'] = 'card bg-danger text-white my-1'
        products['body'] = str(len(response)) + " <i class='fas fa-exclamation-triangle float-right pt-1'></i>"
    else:
        products['header'] = 'card bg-success text-white my-1'
        products['body'] = str(len(response)) + " <i class='far fa-check-circle float-right pt-1'></i>"
    products['thead'] = ['PLU', 'Найменование товара', 'Кол-во продаж за 21 день', 'Средние продажи в день', 'Продано за сегодня', 'Ед. измер']
    products['tbody'] = response
    write_report('low_saled_products_report', products['thead'], products['tbody'], store)
    return products



def get_sellers_perfom(store):
    db = get_bd_ip(store)       
    sellers_perfom = {}
    conn = cx_Oracle.connect("MONITORING/1AAA2BB3D@//{}:1521/ORCL".format(db), encoding="UTF-8", nencoding="UTF-8")
    cur = conn.cursor()
    cur.execute("""
            SELECT 
            user_name,
            SUM(scan_qty) AS sum_scan_qty, --кол-во пиков
            COUNT(id_header) AS count_header --кол-во чеков
            FROM (SELECT id_user, user_name, date_time, scan_qty, id_header
            FROM (SELECT id_user,
                    u.user_name,
                    trunc(h.date_time) as date_time,
                    psq.ext_number AS scan_qty,
                    id_header
                FROM cash.checkheader h
                INNER JOIN cash.checkheader_ext fos
                USING (id_department, id_header, id_paydesk)
                INNER JOIN cash.checkheader_ext lof
                USING (id_department, id_header, id_paydesk)
                INNER JOIN cash.checkheader_ext psq
                USING (id_department, id_header, id_paydesk)
                INNER JOIN cash.checkheader_ext ls
                USING (id_department, id_header, id_paydesk)
                INNER JOIN cash.checkheader_ext pk
                USING (id_department, id_header, id_paydesk)
                INNER JOIN cash.users u
                USING (id_user)
                WHERE id_paydesk < 900
                AND h.operation = 1
                AND fos.ext_name = 'FIRST_OPERATION_START'
                AND lof.ext_name = 'LAST_OPERATION_FINISH'
                AND psq.ext_name = 'PLU_SCAN_QTY'
                AND ls.ext_name = 'LAST_SUBTOTAL'
                AND pk.ext_name = 'POS_KIND'
                AND pk.ext_string = 'POS_REGULAR'
                AND h.date_time > TRUNC(SYSDATE)))
            GROUP BY id_user, user_name, date_time""")
    response = cur.fetchall()
    conn.close()
    if response:
        sellers_perfom['header'] = 'card bg-success text-white my-1'
        sellers_perfom['body'] = str(len(response))
    else:
        sellers_perfom['header'] = 'card bg-success text-white my-1'
        sellers_perfom['body'] = str(len(response)) + " <i class='far fa-check-circle float-right pt-1'></i>"
    sellers_perfom['thead'] = ['ФИО', 'Кол-во сканирований', 'Кол-во чеков']
    sellers_perfom['tbody'] = response
    write_report('sellers_perfom_report', sellers_perfom['thead'], sellers_perfom['tbody'], store)
    return sellers_perfom


def get_stoped_products(store):
    db = get_bd_ip(store)       
    stoped_products = {}
    conn = cx_Oracle.connect("MONITORING/1AAA2BB3D@//{}:1521/ORCL".format(db), encoding="UTF-8", nencoding="UTF-8")
    cur = conn.cursor()
    cur.execute("""select id_item, i.shortname, i.localname, il.dt_last_move
                    from sdd.itemlist il
                    inner join sdd.item i
                    using (id_item)
                    where il.dt_last_move is not null
                    and il.dt_last_move < trunc(sysdate - 3) --сделать -3 если необходимо 72 часа
                    and il.cashprice > 0
                    and il.qty > 0
                    and il.id_department =
                        (SELECT ID_Department FROM SDD.Department WHERE Is_Host = 1)
                    order by il.dt_last_move""")
    response = cur.fetchall()
    conn.close()
    if response:
        stoped_products['header'] = 'card bg-warning text-white my-1'
        stoped_products['body'] = str(len(response)) + " <i class='fas fa-exclamation-triangle float-right pt-1'></i>"
    else:
        stoped_products['header'] = 'card bg-success text-white my-1'
        stoped_products['body'] = str(len(response)) + " <i class='far fa-check-circle float-right pt-1'></i>"
    stoped_products['thead'] = ['ID','Имя', 'Имя+', 'Дата последней продажи']
    stoped_products['tbody'] = response
    write_report('stoped_products_report', stoped_products['thead'], stoped_products['tbody'], store)
    return stoped_products


def get_minus_products(store):
    db = get_bd_ip(store)       
    minus_products = {}
    conn = cx_Oracle.connect("MONITORING/1AAA2BB3D@//{}:1521/ORCL".format(db), encoding="UTF-8", nencoding="UTF-8")
    cur = conn.cursor()
    cur.execute("""select id_item, shortname,localname, cashprice, qty
                from sdd.itemlist
                where cashprice > 0
                and qty < 0""")
    response = cur.fetchall()
    conn.close()
    if response:
        minus_products['header'] = 'card bg-warning text-white my-1'
        minus_products['body'] = str(len(response)) + " <i class='fas fa-exclamation-triangle float-right pt-1'></i>"
    else:
        minus_products['header'] = 'card bg-success text-white my-1'
        minus_products['body'] = str(len(response)) + " <i class='far fa-check-circle float-right pt-1'></i>"
    minus_products['thead'] = ['ID', 'Имя', 'Имя+', 'Цена', 'Кол-во']
    minus_products['tbody'] = response
    write_report('minus_products_report', minus_products['thead'], minus_products['tbody'], store)
    return minus_products