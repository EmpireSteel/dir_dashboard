﻿function get_alcohol_errors(store) {
    $.ajax({
        url: "/store/" + store + "/get_alcohol_errors/",
        cache: false,
        contentType: 'application/json;charset=UTF-8',
        'dataType': 'json',
        success: function (data) {
            $("#alcohol_errors_header").attr('class', data.header);
            $("#alcohol_errors_body").html(data.body);  
            $("#alcohol_errors_modal_body").html(data.modal);          
        }
    });
}

function get_cashless(store) {
    $.ajax({
        url: "/store/" + store + "/get_cashless/",
        cache: false,
        contentType: 'application/json;charset=UTF-8',
        'dataType': 'json',
        success: function (data) {
            // $("#business_cashless_header").attr('class', data.header);
            // $("#business_cashless_body").html(data.body);
            $("#services_cashless").attr('class', data.services_header);
            $("#services_cashless").attr('data-original-title', data.services_tooltip);
            
        }
    });
}

function get_poses(store) {
    $.ajax({
        url: "/store/" + store + "/get_poses/",
        cache: false,
        contentType: 'application/json;charset=UTF-8',
        'dataType': 'json',
        success: function (data) {
            poses = '<div class="' + data.header + '" id="headingPoses" tabindex="0" data-toggle="collapse" data-target="#poses" aria-expanded="true" aria-controls="poses">';
            poses +=  '<h5 class=" mb-0"><i class="fas fa-cash-register pt-1"></i> Кассы (' + data.poses.length + ')</h5></div>';
            if (poses.header != "card-header bg-success text-white"){
                poses += '<div id="poses" class="collapse show" aria-labelledby="headingPoses">';
            }
            else {
                poses += '<div id="poses" class="collapse" aria-labelledby="headingPoses">';
            }
            poses += '<div class="card-body-block bg-light"><div class="row">';
            $.each(data.poses, function (index, item) {
                poses += '<div class="col-xl-2 col-lg-3">';
                poses += '<div class="' + item.typ + '" text-white my-1 ">';
                if (item.typ == "card bg-success text-white my-1") {
                    poses += '<div class="card-body-block" tabindex="0" data-toggle="popover" data-placement="left" data-html="true" ';
                    poses += 'data-content="Наличные: ' + item.cash + ' ₽<br />Безналичные: ' + item.cashless + ' ₽<br />';
                    poses += 'Смешанный: ' + item.mixed + ' ₽<br />Возврат: ' + item.returns + ' ₽<br />';
                    poses += 'Всего: ' + item.summa + ' ₽">';
                }
                else {
                    poses += '<div class="card-body-block" tabindex="0">';
                }
                poses += '<p class="text-center mb-0">' + item.name + ' </p></div></div></div>';
            })
            poses += '</div></div></div>';
            $("#poses_card").html(poses);  
            $('[data-toggle="popover"]').popover()  
        }
    });
}

function get_kso(store) {
    $.ajax({
        url: "/store/" + store + "/get_kso/",
        cache: false,
        contentType: 'application/json;charset=UTF-8',
        'dataType': 'json',
        success: function (data) {
            if (data.kso.length != 0) {
                kso = '<div class="' + data.header + '" id="headingKso" tabindex="0" data-toggle="collapse" data-target="#kso" aria-expanded="true" aria-controls="kso">';
                kso +=  '<h5 class=" mb-0"><i class="fas fa-cash-register pt-1"></i> КСО (' + data.kso.length + ')</h5></div>';
                if (kso.header != "card-header bg-success text-white"){
                    kso += '<div id="kso" class="collapse show" aria-labelledby="headingKso">';
                }
                else {
                    kso += '<div id="kso" class="collapse" aria-labelledby="headingKso">';
                }
                kso += '<div class="card-body-block bg-light"><div class="row">';
                $.each(data.kso, function (index, item) {
                    kso += '<div class="col-xl-2 col-lg-3">';
                    kso += '<div class="' + item.typ + '" text-white my-1 ">';
                    if (item.typ == "card bg-success text-white my-1") {
                        kso += '<div class="card-body-block" tabindex="0" data-toggle="popover" data-placement="left" data-html="true" data-content="Наличные: ' + item.cash + ' ₽ <br />Безналичные: ' + item.cashless + ' ₽<br />Всего: ' + item.summa + ' ₽<br />">';
                    }
                    else {
                        kso += '<div class="card-body-block" tabindex="0">';
                    }
                    kso += '<p class="text-center mb-0">' + item.name + ' </p></div></div></div>';
                })
                kso += '</div></div></div>';
                $("#kso_card").html(kso); 
            }   
            else {
                $("#kso_card").html('');   
                $(".popover").popover("hide");
                $('[data-toggle="popover"]').popover() 
            }
        }
    });
}
    
function get_scales(store) {
    $.ajax({
        url: "/store/" + store + "/get_scales/",
        cache: false,
        contentType: 'application/json;charset=UTF-8',
        'dataType': 'json',
        success: function (data) {
            scales = '<div class="' + data.header + '" id="headingScales" tabindex="0" data-toggle="collapse" data-target="#scales" aria-expanded="true" aria-controls="scales">';
            scales += '<h5 class=" mb-0"><i class="fas fa-balance-scale pt-1"></i> Весы (' + data.scales.length + ')</h5></div>';
            if (scales.header != "card-header bg-success text-white"){
                scales += '<div id="scales" class="collapse show" aria-labelledby="headingScales">';
            }
            else {
                scales += '<div id="scales" class="collapse" aria-labelledby="headingScales">';
            }
            scales += '<div class="card-body-block bg-light"><div class="row">';
            $.each(data.scales, function (index, scale) {
                scales += '<div class="col-xl-3 col-lg-6 col-md-12">';
                scales += '<div class="' + scale.typ + '" text-white my-1 ">';
                if (scale.typ != "card bg-success text-white my-1") {
                    scales += '<div class="card-body-block" tabindex="0" data-toggle="tooltip" data-placement="top" data-html="true" ';
                    if (scale.typ == "card bg-dark text-white my-1") {
                        scales += 'title="Весы не в сети">';
                    }
                    else if (scale.typ == "card bg-warning text-white my-1") {
                        scales += 'title="Прогрузка больше 6 часов назад">';
                    }
                    else if (scale.typ == "card bg-danger text-white my-1") {
                        scales += 'title="Прогрузка больше 12 часов назад">';
                    }
                }
                else {
                    scales += '<div class="card-body-block" tabindex="0">';
                }
                scales += '<p class="text-center mb-0">' + scale.name + '</p></div></div></div>';
            })
            scales += '</div></div></div>';
            $("#scales_card").html(scales);
            $(".tooltip").tooltip("hide");
            $('[data-toggle="tooltip"]').tooltip()
            
        }
    });
}

function get_loyalty(store) {
    $.ajax({
        url: "/store/" + store + "/get_loyalty/",
        cache: false,
        contentType: 'application/json;charset=UTF-8',
        'dataType': 'json',
        success: function (data) {
            $("#services_loyalty").attr('class', data.services_header);
            $("#services_loyalty").attr('data-original-title', data.services_tooltip);
        }
    });
}

function get_services_alcohol(store) {
    $.ajax({
        url: "/store/" + store + "/get_services_alcohol/",
        cache: false,
        contentType: 'application/json;charset=UTF-8',
        'dataType': 'json',
        success: function (data) {
            $("#services_alcohol").attr('class', data.header);
            $("#services_alcohol").attr('data-original-title', data.tooltip);
            
            
        }
    });
}

function get_rx(store) {
    $.ajax({
        url: "/store/" + store + "/get_rx/",
        cache: false,
        contentType: 'application/json;charset=UTF-8',
        'dataType': 'json',
        success: function (data) {
            $("#services_rx_header").attr('class', data.header);
            $("#services_rx_header").attr('data-original-title', data.services_tooltip);
            $("#business_rx_header").attr('class', data.header);
            $("#business_rx_tooltip").attr('data-original-title', data.business_tooltip);
            $("#business_rx_body").html(data.body); 
            
            
        }
    });
}


function get_business_revenue(store) {
    $.ajax({
        url: "/store/" + store + "/get_business_revenue/",
        cache: false,
        contentType: 'application/json;charset=UTF-8',
        'dataType': 'json',
        success: function (data) {
            $("#business_revenue_header").attr('class', data.header);
            $("#business_revenue_body").html(data.body);      
            $("#business_revenue_tooltip").attr('data-original-title', data.tooltip);     
        }
    });
}


function get_sellers_perfom(store) {
    $.ajax({
        url: "/store/" + store + "/get_sellers_perfom/",
        cache: false,
        contentType: 'application/json;charset=UTF-8',
        'dataType': 'json',
        success: function (data) {
            $("#sellers_perfom_header").attr('class', data.header);     
            $("#sellers_perfom_body").html(data.body);
            if (data.tbody.length > 0){
                var table = '<table id="sellers_perfom_table" class="table table-striped table-bordered" cellspacing="0" width="100%"><thead><tr>';     
                for (var i=0;i<data.thead.length; i++){
                    table +='<th>';
                    table +=data.thead[i];
                    table +='</th>';
                }
                table+='</tr></thead><tbody>';
                for (var i=0;i<data.tbody.length; i++){
                    table+='<tr>';
                    var line = data.tbody[i];
                    for (var j=0;j<line.length; j++){
                        table+='<td>';
                        table+=line[j];
                        table+='</td>';
                    }
                    table+='</tr>';
                }
                table+='</tbody></table>';
                $('#sellers_perfom_modal_body').html(table);
            }
            else {
                $('#sellers_perfom_modal_body').html('');
            }
            $('#sellers_perfom_table').DataTable();
            $('.dataTables_length').addClass('bs-select');
        }
    });
}



function get_services_net(store) {
    $.ajax({
        url: "/store/" + store + "/get_services_net/",
        cache: false,
        contentType: 'application/json;charset=UTF-8',
        'dataType': 'json',
        success: function (data) {
            $("#services_net").attr('class', data.header);     
            $("#services_net").attr('data-original-title', data.tooltip);     
        }
    });
}

function get_overdue_products(store) {
    $.ajax({
        url: "/store/" + store + "/get_overdue_products/",
        cache: false,
        contentType: 'application/json;charset=UTF-8',
        'dataType': 'json',
        success: function (data) {
            $("#overdue_products_header").attr('class', data.header);     
            $("#overdue_products_body").html(data.body);
            if (data.tbody.length > 0){
                var table = '<table id="overdue_products_table" class="table table-striped table-bordered" cellspacing="0" width="100%"><thead><tr>';     
                for (var i=0;i<data.thead.length; i++){
                    table +='<th>';
                    table +=data.thead[i];
                    table +='</th>';
                }
                table+='</tr></thead><tbody>';
                for (var i=0;i<data.tbody.length; i++){
                    table+='<tr>';
                    var line = data.tbody[i];
                    for (var j=0;j<line.length; j++){
                        table+='<td>';
                        table+=line[j];
                        table+='</td>';
                    }
                    table+='</tr>';
                }
                table+='</tbody></table>';
                $('#overdue_products_modal_body').html(table);
            }
            else {
                $('#overdue_products_modal_body').html('');
            }
            $('#overdue_products_table').DataTable();
            $('.dataTables_length').addClass('bs-select');
        }
    });
}

function get_low_saled_products(store) {
    $.ajax({
        url: "/store/" + store + "/get_low_saled_products/",
        cache: false,
        contentType: 'application/json;charset=UTF-8',
        'dataType': 'json',
        success: function (data) {
            $("#low_saled_products_header").attr('class', data.header);     
            $("#low_saled_products_body").html(data.body);
            if (data.tbody.length > 0){
                var table = '<table id="low_saled_products_table" class="table table-striped table-bordered" cellspacing="0" width="100%"><thead><tr>';     
                for (var i=0;i<data.thead.length; i++){
                    table +='<th>';
                    table +=data.thead[i];
                    table +='</th>';
                }
                table+='</tr></thead><tbody>';
                for (var i=0;i<data.tbody.length; i++){
                    table+='<tr>';
                    var line = data.tbody[i];
                    for (var j=0;j<line.length; j++){
                        table+='<td>';
                        table+=line[j];
                        table+='</td>';
                    }
                    table+='</tr>';
                }
                table+='</tbody></table>';
                $('#low_saled_products_modal_body').html(table);
            }
            else {
                $('#low_saled_products_modal_body').html('');
            }
            $('#low_saled_products_table').DataTable();
            $('.dataTables_length').addClass('bs-select');
        }
    });
}

function get_stoped_products(store) {
    $.ajax({
        url: "/store/" + store + "/get_stoped_products/",
        cache: false,
        contentType: 'application/json;charset=UTF-8',
        'dataType': 'json',
        success: function (data) {
            $("#stoped_products_header").attr('class', data.header);     
            $("#stoped_products_body").html(data.body);
            if (data.tbody.length > 0){
                var table = '<table id="stoped_products_table" class="table table-striped table-bordered" cellspacing="0" width="100%"><thead><tr>';     
                for (var i=0;i<data.thead.length; i++){
                    table +='<th>';
                    table +=data.thead[i];
                    table +='</th>';
                }
                table+='</tr></thead><tbody>';
                for (var i=0;i<data.tbody.length; i++){
                    table+='<tr>';
                    var line = data.tbody[i];
                    for (var j=0;j<line.length; j++){
                        table+='<td>';
                        table+=line[j];
                        table+='</td>';
                    }
                    table+='</tr>';
                }
                table+='</tbody></table>';
                $('#stoped_products_modal_body').html(table);
            }
            else {
                $('#stoped_products_modal_body').html('');
            }
            $('#stoped_products_table').DataTable();
            $('.dataTables_length').addClass('bs-select');
        }
    });
}



function get_minus_products(store) {
    $.ajax({
        url: "/store/" + store + "/get_minus_products/",
        cache: false,
        contentType: 'application/json;charset=UTF-8',
        'dataType': 'json',
        success: function (data) {
            $("#minus_products_header").attr('class', data.header);     
            $("#minus_products_body").html(data.body);
            if (data.tbody.length > 0){
                var table = '<table id="minus_products_table" class="table table-striped table-bordered" cellspacing="0" width="100%"><thead><tr>';     
                for (var i=0;i<data.thead.length; i++){
                    table +='<th>';
                    table +=data.thead[i];
                    table +='</th>';
                }
                table+='</tr></thead><tbody>';
                for (var i=0;i<data.tbody.length; i++){
                    table+='<tr>';
                    var line = data.tbody[i];
                    for (var j=0;j<line.length; j++){
                        table+='<td>';
                        table+=line[j];
                        table+='</td>';
                    }
                    table+='</tr>';
                }
                table+='</tbody></table>';
                $('#minus_products_modal_body').html(table);
            }
            else {
                $('#minus_products_modal_body').html('');
            }
            $('#minus_products_table').DataTable();
            $('.dataTables_length').addClass('bs-select');
        }
    });
}



function get_alcohol_documents(store) {
    $.ajax({
        url: "/store/" + store + "/get_alcohol_documents/",
        cache: false,
        contentType: 'application/json;charset=UTF-8',
        'dataType': 'json',
        success: function (data) {
            $("#alcohol_documents_header").attr('class', data.header);
            $("#alcohol_documents_body").html(data.body);          
        }
    });
}


function get_business_theme() {
    var business_revenue = $('#business_revenue_header').attr('class');
    var alcohol_errors = $('#alcohol_documents_header').attr('class');
    var sellers_perfom = $('#sellers_perfom_header').attr('class');
    if (business_revenue.indexOf("danger") >= 0 ||  alcohol_errors.indexOf("danger") >= 0 ||  sellers_perfom.indexOf("danger") >= 0) {
        $("#headingBusiness").attr('class', 'card-header bg-danger text-white');
    }
    else if (business_revenue.indexOf("warning") >= 0 ||  alcohol_errors.indexOf("warning") >= 0 ||  sellers_perfom.indexOf("warning") >= 0) {
        $("#headingBusiness").attr('class', 'card-header bg-warning text-white');
    }
    else if (business_revenue.indexOf("success") >= 0 &&  alcohol_errors.indexOf("success") >= 0 &&  sellers_perfom.indexOf("success") >= 0) {
        $("#headingBusiness").attr('class', 'card-header bg-success text-white');
    }
}


function get_services_theme() {
    var services_alcohol = $('#services_alcohol').attr('class');
    var services_rx = $('#services_rx_header').attr('class');
    var services_loyalty = $('#services_loyalty').attr('class');
    var services_cashless = $('#services_cashless').attr('class');
    var services_net = $('#services_net').attr('class');
    if (services_alcohol.indexOf("danger") >= 0 ||  services_rx.indexOf("danger") >= 0  ||  services_loyalty.indexOf("danger") >= 0 ||  services_cashless.indexOf("danger") >= 0 ||  services_net.indexOf("danger") >= 0) {
        $("#headingServices").attr('class', 'card-header bg-danger text-white');
    }
    else if (services_alcohol.indexOf("warning") >= 0 ||  services_rx.indexOf("warning") >= 0  ||  services_loyalty.indexOf("warning") >= 0 ||  services_cashless.indexOf("warning") >= 0 ||  services_net.indexOf("warning") >= 0) {
        $("#headingServices").attr('class', 'card-header bg-warning text-white');
    }
    else if (services_alcohol.indexOf("success") >= 0 &&  services_rx.indexOf("success") >= 0  &&  services_loyalty.indexOf("success") >= 0 &&  services_cashless.indexOf("success") >= 0 &&  services_net.indexOf("success") >= 0) {
        $("#headingServices").attr('class', 'card-header bg-success text-white');
    }
}


function get_products_theme() {
    var business_rx = $('#business_rx_header').attr('class');
    var overdue_products = $('#overdue_products_header').attr('class');
    var alcohol_errors = $('#alcohol_errors_header').attr('class');
    var low_saled_products = $('#low_saled_products_header').attr('class');
    var minus_products = $('#minus_products_header').attr('class');
    var stoped_products = $('#stoped_products_header').attr('class');
    if (business_rx.indexOf("danger") >= 0 ||  overdue_products.indexOf("danger") >= 0  ||  alcohol_errors.indexOf("danger") >= 0 ||  low_saled_products.indexOf("danger") >= 0 ||  minus_products.indexOf("danger") >= 0 ||  stoped_products.indexOf("danger") >= 0) {
        $("#headingProducts").attr('class', 'card-header bg-danger text-white');
    }
    else if (business_rx.indexOf("warning") >= 0 ||  overdue_products.indexOf("warning") >= 0  ||  alcohol_errors.indexOf("warning") >= 0 ||  low_saled_products.indexOf("warning") >= 0 ||  minus_products.indexOf("warning") >= 0 ||  stoped_products.indexOf("warning") >= 0) {
        $("#headingProducts").attr('class', 'card-header bg-warning text-white');
    }
    else if (business_rx.indexOf("success") >= 0 &&  overdue_products.indexOf("success") >= 0  &&  alcohol_errors.indexOf("success") >= 0 &&  low_saled_products.indexOf("success") >= 0 &&  minus_products.indexOf("success") >= 0 &&  stoped_products.indexOf("success") >= 0) {
        $("#headingProducts").attr('class', 'card-header bg-success text-white');
    }
}

function get_data() {
    var store = $(location).attr('href').split('/');
    store = store[store.length-2].split('-') ;
    store = store[store.length-1];
    get_business_theme();
    get_products_theme();
    get_services_theme();

    get_poses(store);
    get_kso(store);
    get_scales(store);

    get_business_revenue(store);
    get_alcohol_errors(store);
    get_sellers_perfom(store);
    

    get_cashless(store); 
    get_services_net(store);
    get_services_alcohol(store);
    get_loyalty(store);
    get_rx(store);
    
    get_low_saled_products(store);
    get_alcohol_documents(store);
    get_overdue_products(store);
    get_minus_products(store);
    get_stoped_products(store);
    

    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = time;
    $("#last_update").html(dateTime);
    $("#update_sign").attr('class', 'fas fa-sync float-right pt-1');

}


$(document).ready(function () {
    setTimeout('get_data()', 2000);
    setInterval('get_data()', 900000);
    setInterval('get_business_theme()', 1000);
    setInterval('get_services_theme()', 1000);
    setInterval('get_products_theme()', 1000);
});  






